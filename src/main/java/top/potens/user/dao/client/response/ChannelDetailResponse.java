package top.potens.user.dao.client.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.potens.core.annotation.JsonFormatDatetime;

import java.util.Date;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ChannelDetailResponse
 * @projectName cms-api
 * @date 2020/1/30 15:03
 */
@Data
public class ChannelDetailResponse {
    @ApiModelProperty(value = "渠道", name = "channelId", required = true, example = "1")
    private Integer channelId;

    @ApiModelProperty(value = "创建时间", name = "createTime", required = true, example = "2019-01-01 01:01:01.222")
    @JsonFormatDatetime
    private Date createTime;

    @ApiModelProperty(value = "更新时间", name = "updateTime", required = true, example = "2019-01-01 01:01:01.222")
    @JsonFormatDatetime
    private Date updateTime;

    @ApiModelProperty(value = "渠道名称", name = "channelName", required = true, example = "")
    private String channelName;

    @ApiModelProperty(value = "渠道码", name = "channelCode", required = true, example = "")
    private String channelCode;

    @ApiModelProperty(value = "对应的member是否可登陆", name = "userCanLogin", required = true, example = "0")
    private Integer userCanLogin;

    @ApiModelProperty(value = "member是否检查credential", name = "userCheckCredential", required = true, example = "0")
    private Integer userCheckCredential;
}
