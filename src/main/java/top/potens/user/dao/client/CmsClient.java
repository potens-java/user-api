package top.potens.user.dao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import top.potens.core.model.ApiResult;
import top.potens.user.dao.client.response.ChannelDetailResponse;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className CmsClient
 * @projectName user-api
 * @date 2020/1/30 15:43
 */
@FeignClient(name = "cms-api", url = "http://127.0.0.1:20002")
public interface CmsClient {
    /**
    *
    * 方法功能描述:
    *
    * @author yanshaowen
    * @date 2020/1/30 15:47
    * @param channelCode
    * @return
    * @throws
    */
    @GetMapping("/channel/by-code")
    ApiResult<ChannelDetailResponse> getChannelByCode(@RequestParam("channelCode") String channelCode);
}
