package top.potens.user.dao.db.ext;

import top.potens.user.model.UserAuth;

public interface UserAuthExtMapper {
    /**
    *
    * 方法功能描述: 包含主键的插入
    *
    * @author yanshaowen
    * @date 2020/2/1 14:08
    * @param record
    * @return
    * @throws
    */
    int insertCompleteSelective(UserAuth record);
}