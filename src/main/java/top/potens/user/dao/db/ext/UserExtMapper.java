package top.potens.user.dao.db.ext;

import org.apache.ibatis.annotations.Param;
import top.potens.user.model.User;
import top.potens.user.model.UserExample;

import java.util.List;

public interface UserExtMapper {
    /**
    *
    * 方法功能描述: 包含主键的插入
    *
    * @author yanshaowen
    * @date 2020/2/1 14:08
    * @param record
    * @return
    * @throws
    */
    int insertCompleteSelective(User record);
}