package top.potens.user.curd;

import top.potens.core.service.TableCommonService;
import top.potens.user.model.UserAuth;

import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserAuthCurd
 * @projectName user-api
 * @date 2020/1/19 17:06
 */
public interface UserAuthCurd extends TableCommonService<UserAuth, Long> {
    /**
    *
    * 方法功能描述: 根据用户id查询userAuth列表
    *
    * @author yanshaowen
    * @date 2020/1/19 17:26
    * @param userId
    * @return
    * @throws
    */
    List<UserAuth> selectListByUserId(Long userId);

    /**
     *
     * 方法功能描述: 根据用户身份查询userAuth
     *
     * @author yanshaowen
     * @date 2020/1/19 17:26
     * @param channelId
     * @param identifier
     * @return
     * @throws
     */
    List<UserAuth> selectListByIdentifier(Integer channelId, String identifier);
}
