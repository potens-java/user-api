package top.potens.user.curd.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.potens.core.service.impl.AbstractSimpleTableCommonServiceImpl;
import top.potens.user.curd.UserAuthCurd;
import top.potens.user.dao.db.auto.UserAuthMapper;
import top.potens.user.model.UserAuth;
import top.potens.user.model.UserAuthExample;

import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserCurdImpl
 * @projectName user-api
 * @date 2020/1/19 16:49
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAuthCurdImpl extends AbstractSimpleTableCommonServiceImpl<UserAuth, Long> implements UserAuthCurd {
    private final UserAuthMapper userAuthMapper;
    @Override
    protected UserAuth mapperByPrimaryKey(Long id) {
        return userAuthMapper.selectByPrimaryKey(id);
    }

    @Override
    protected UserAuth mapperBySecondPrimaryKey(Long id) {
        return null;
    }

    @Override
    protected Boolean isDelete(UserAuth userAuth) {
        return false;
    }

    @Override
    public List<UserAuth> selectListByUserId(Long userId) {
        UserAuthExample userAuthExample = new UserAuthExample();
        userAuthExample.createCriteria().andUserIdEqualTo(userId);
        return userAuthMapper.selectByExample(userAuthExample);
    }

    @Override
    public List<UserAuth> selectListByIdentifier(Integer channelId, String identifier) {
        UserAuthExample userAuthExample = new UserAuthExample();
        userAuthExample.createCriteria()
                .andChannelIdEqualTo(channelId)
                .andIdentifierEqualTo(identifier);
        return userAuthMapper.selectByExample(userAuthExample);
    }
}
