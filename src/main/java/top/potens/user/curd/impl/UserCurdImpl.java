package top.potens.user.curd.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.potens.core.service.impl.AbstractSimpleTableCommonServiceImpl;
import top.potens.core.util.BeanCopierUtil;
import top.potens.core.util.SnowflakeIdWorker;
import top.potens.user.bmo.UserMoreAuthBo;
import top.potens.user.common.constant.CommonConstant;
import top.potens.user.curd.UserCurd;
import top.potens.user.dao.db.auto.UserAuthMapper;
import top.potens.user.dao.db.auto.UserMapper;
import top.potens.user.dao.db.ext.UserAuthExtMapper;
import top.potens.user.dao.db.ext.UserExtMapper;
import top.potens.user.model.User;
import top.potens.user.model.UserAuth;
import top.potens.user.model.UserExample;

import java.math.BigDecimal;
import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserCurdImpl
 * @projectName user-api
 * @date 2020/1/19 16:49
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserCurdImpl extends AbstractSimpleTableCommonServiceImpl<User, Long> implements UserCurd {
    private final UserMapper userMapper;
    private final UserAuthMapper userAuthMapper;
    private final UserExtMapper userExtMapper;
    private final UserAuthExtMapper userAuthExtMapper;
    @Override
    protected User mapperByPrimaryKey(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    protected User mapperBySecondPrimaryKey(Long id) {
        return null;
    }

    @Override
    protected Boolean isDelete(User user) {
        return false;
    }

    @Override
    public UserMoreAuthBo selectDetailById(Long userId) {
        UserMoreAuthBo userMoreAuthBo = new UserMoreAuthBo();
        User user = this.byPrimaryKeyException(userId);
        BeanCopierUtil.convert(user, userMoreAuthBo);

        return userMoreAuthBo;
    }

    @Override
    public List<User> selectListByUserIdList(List<Long> userIdList) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUserIdIn(userIdList).andIsDeleteEqualTo(CommonConstant.IsDelete.NO);
        return userMapper.selectByExample(userExample);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertOne(User user, UserAuth userAuth) {
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);
        long userId = idWorker.nextId();
        long userAuthId = idWorker.nextId();
        user.setUserId(userId);
        userAuth.setUserAuthId(userAuthId);
        userAuth.setUserId(userId);
        userExtMapper.insertCompleteSelective(user);
        userAuthExtMapper.insertCompleteSelective(userAuth);
    }
}
