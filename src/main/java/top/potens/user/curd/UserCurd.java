package top.potens.user.curd;

import top.potens.core.service.TableCommonService;
import top.potens.user.bmo.UserMoreAuthBo;
import top.potens.user.model.User;
import top.potens.user.model.UserAuth;

import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserCurd
 * @projectName user-api
 * @date 2020/1/19 16:48
 */
public interface UserCurd extends TableCommonService<User, Long> {
    /**
     *
     * 方法功能描述: 按用户id查询用户的具体详情
     *
     * @author yanshaowen
     * @date 2020/1/19 17:16
     * @param userId
     * @return
     * @throws
     */
    UserMoreAuthBo selectDetailById(Long userId);

    /**
    *
    * 方法功能描述:
    *
    * @author yanshaowen
    * @date 2020/1/28 14:31
    * @param userIdList
    * @return
    * @throws
    */
    List<User> selectListByUserIdList(List<Long> userIdList);

    /**
    *
    * 方法功能描述:
    *
    * @author yanshaowen
    * @date 2020/1/29 17:49
    * @param user
    * @param userAuth
    * @return
    * @throws
    */
    void insertOne(User user, UserAuth userAuth);

}
