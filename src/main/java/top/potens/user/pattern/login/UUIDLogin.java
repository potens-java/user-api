package top.potens.user.pattern.login;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.potens.core.exception.ApiException;
import top.potens.core.util.StringUtil;
import top.potens.user.code.UserCode;
import top.potens.user.model.UserAuth;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UUIDLogin
 * @projectName user-api
 * @date 2020/1/28 14:43
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UUIDLogin extends AbstractTemplateLogin {
    @Override
    void verifyParamsLawful() {
        if (StringUtil.isBlank(this.getRequest().getIdentifier())) {
            throw new ApiException(UserCode.USER_PARAMS_ERROR);
        }
    }

    @Override
    void verifyPassword(UserAuth userAuth) {
    }

    @Override
    String getNickname() {
        return "";
    }

}
