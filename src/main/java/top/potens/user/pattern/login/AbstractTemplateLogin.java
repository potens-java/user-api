package top.potens.user.pattern.login;

import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.potens.core.constant.TokenConstant;
import top.potens.core.exception.ApiException;
import top.potens.core.model.ApiResult;
import top.potens.core.model.TokenUser;
import top.potens.core.serialization.JSON;
import top.potens.core.util.ApiUtil;
import top.potens.core.util.SpringUtil;
import top.potens.core.util.StringUtil;
import top.potens.core.util.TokenUtil;
import top.potens.log.AppLogger;
import top.potens.log.HttpContext;
import top.potens.user.code.UserCode;
import top.potens.user.curd.UserAuthCurd;
import top.potens.user.curd.UserCurd;
import top.potens.user.dao.client.CmsClient;
import top.potens.user.dao.client.response.ChannelDetailResponse;
import top.potens.user.model.User;
import top.potens.user.model.UserAuth;
import top.potens.user.request.UserLoginCommonRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className AbstractTemplateLogin
 * @projectName user-api
 * @date 2020/1/28 11:50
 */
@Service
abstract public class AbstractTemplateLogin {
    private UserLoginCommonRequest request;
    private Integer channelId;
    @Autowired
    private UserAuthCurd userAuthCurd;
    @Autowired
    private UserCurd userCurd;
    @Autowired
    private CmsClient cmsClient;

    protected UserAuth getUserAuth() {
        String credential = this.request.getCredential();
        String identifier = this.request.getIdentifier();
        List<UserAuth> userAuthList = userAuthCurd.selectListByIdentifier(channelId, identifier);

        if (userAuthList.isEmpty()) {
            AppLogger.info("查询用户auth 不存在 identityType:[{}] channelId:[{}] credential:[{}]", channelId, identifier, credential);
            return null;
        }

        // 批量查user 判断isDelete
        List<Long> userIds = userAuthList.stream().map(UserAuth::getUserId).collect(Collectors.toList());
        List<User> users = userCurd.selectListByUserIdList(userIds);
        Map<Long, User> userGroupBy = users.stream().collect(Collectors.toMap(User::getUserId, user -> user));

        ArrayList<UserAuth> userAuthExclude = new ArrayList<>();

        userAuthList.forEach(userAuth -> {
            if (userGroupBy.containsKey(userAuth.getUserId())) {
                userAuthExclude.add(userAuth);
            }
        });
        if (userAuthExclude.size() != 1) {
            AppLogger.error("查询用户auth 存在多个 identityType:[{}] channelId:[{}] credential:[{}] UserAuthList:[{}]", channelId, identifier, credential, JSON.toJSONString(userAuthList));
            throw new ApiException(UserCode.USER_EXIST_MORE);
        }
        return userAuthExclude.get(0);
    }
    protected Integer channelCodeToId(String channelCode) {
        AppLogger.info("查询channelId channelCode:[{}]", channelCode);
        ApiResult<ChannelDetailResponse> result = cmsClient.getChannelByCode(channelCode);
        AppLogger.info("查询channelId channelCode:[{}] result:[{}]", channelCode, JSON.toJSONString(result));
        ChannelDetailResponse data = ApiUtil.getData(result);
        return data.getChannelId();
    }
    /**
    *
    * 方法功能描述: 验证参数是否合法
    *
    * @author yanshaowen
    * @date 2020/1/29 14:30
    * @return
    * @throws
    */
    abstract void verifyParamsLawful();
    /**
    *
    * 方法功能描述: 验证密码是否正确
    *
    * @author yanshaowen
    * @date 2020/1/30 14:14
    * @param userAuth
    * @return
    * @throws
    */
    abstract void verifyPassword(UserAuth userAuth);

    abstract String getNickname();
    /**
    *
    * 方法功能描述: 插入用户
    *
    * @author yanshaowen
    * @date 2020/1/30 14:16
    * @param
    * @return
    * @throws
    */
    protected Long createUser(){
        User user = new User();
        user.setNickname(getNickname());
        UserAuth userAuth = new UserAuth();
        userAuth.setChannelId(channelId);
        userAuth.setIdentifier(request.getIdentifier());
        userAuth.setCredential(request.getCredential());
        userCurd.insertOne(user, userAuth);
        return user.getUserId();
    }
    protected String updateToken(TokenUser tokenUser) {
        String uuid = StringUtil.getUuid();
        RedissonClient bean = SpringUtil.getBean(RedissonClient.class);
        // 查找有没有对应uid 的旧token
        RBucket<String> userTokenBucket = bean.getBucket(TokenConstant.USER_TOKEN + tokenUser.getUserId());
        String oldTokenKey = userTokenBucket.getAndDelete();
        // 存在旧的token就删除
        if (oldTokenKey != null) {
            RBucket<Object> bucket = bean.getBucket(TokenConstant.TOKEN_REDISSION_NAME + oldTokenKey);
            bucket.delete();
        }
        // 设置新的uid和token的对应关系
        userTokenBucket.set(uuid, TokenConstant.TOKEN_EXPIRE_TIME, TimeUnit.SECONDS);
        RBucket<Object> bucket = bean.getBucket(TokenConstant.TOKEN_REDISSION_NAME + uuid);
        bucket.set(tokenUser, TokenConstant.TOKEN_EXPIRE_TIME, TimeUnit.SECONDS);

        Cookie cookie = new Cookie(TokenConstant.TOKEN_NAME, uuid);
        cookie.setPath("/");
        cookie.setMaxAge(TokenConstant.TOKEN_EXPIRE_TIME);
        HttpServletResponse response = HttpContext.getResponse();
        response.addCookie(cookie);
        return uuid;
    }

    public String auto(UserLoginCommonRequest request) {
        this.request = request;
        // 验证参数是否合法
        verifyParamsLawful();
        // channelCode转换为channelId
        channelId = channelCodeToId(request.getChannelCode());
        // 查询用户是否存在数据库
        UserAuth userAuth = getUserAuth();
        Long userId = null;
        if (userAuth != null) {
            // 如果存在则判断密码是否正确
            verifyPassword(userAuth);
            userId = userAuth.getUserId();
        } else {
            // 如果不存在则创建用户
            userId = createUser();
        }
        User user = userCurd.byPrimaryKey(userId);
        TokenUser tokenUser = new TokenUser();
        tokenUser.setUsername(user.getNickname());
        tokenUser.setUserId(user.getUserId());
        return updateToken(tokenUser);
    }

    public UserLoginCommonRequest getRequest() {
        return request;
    }

    public Integer getChannelId() {
        return channelId;
    }
}
