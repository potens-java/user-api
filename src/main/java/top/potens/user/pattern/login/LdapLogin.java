package top.potens.user.pattern.login;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;
import top.potens.core.exception.ApiException;
import top.potens.core.serialization.JSON;
import top.potens.core.util.StringUtil;
import top.potens.log.AppLogger;
import top.potens.user.bmo.Person;
import top.potens.user.code.UserCode;
import top.potens.user.config.ApolloConfiguration;
import top.potens.user.mapper.PersonAttributeMapper;
import top.potens.user.model.UserAuth;

import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className LdapLogin
 * @projectName user-api
 * @date 2020/2/1 14:24
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LdapLogin extends AbstractTemplateLogin {
    private final ApolloConfiguration apolloConfiguration;
    private final LdapTemplate ldapTemplate;
    private Person person;

    @Override
    void verifyParamsLawful() {
        if (StringUtil.isBlank(this.getRequest().getIdentifier())) {
            throw new ApiException(UserCode.USER_PARAMS_ERROR);
        }
        if (StringUtil.isBlank(this.getRequest().getCredential())) {
            throw new ApiException(UserCode.USER_PARAMS_ERROR);
        }
    }

    @Override
    void verifyPassword(UserAuth userAuth) {
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(apolloConfiguration.getLdapIdentifier(), this.getRequest().getIdentifier()));
        boolean authenticate = ldapTemplate.authenticate("", filter.encode(), this.getRequest().getCredential());
        if (!authenticate) {
            AppLogger.warn("ldap登录 用户名或密码错误 username:[{}] password:[{}]", this.getRequest().getIdentifier(), this.getRequest().getCredential());
            throw new ApiException(UserCode.USERNAME_OR_PASSWORD_ERROR);
        }
        List<Person> personList = ldapTemplate.search("", filter.encode(), new PersonAttributeMapper());
        if (CollectionUtils.isEmpty(personList) || personList.size() != 1) {
            AppLogger.warn("ldap登录 用户不存在或存在多个 username:[{}] password:[{}] personList:[{}]", this.getRequest().getIdentifier(), this.getRequest().getCredential(), JSON.toJSONString(personList));
            throw new ApiException(UserCode.USERNAME_OR_PASSWORD_ERROR);
        }
        person = personList.get(0);
    }

    @Override
    String getNickname() {
        if (person == null){
            AppLogger.warn("ldap登录 person为null nickname为默认值");
            return null;
        }
        return person.getCn();
    }
}
