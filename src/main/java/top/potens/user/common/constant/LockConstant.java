package top.potens.user.common.constant;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className LockConstant
 * @projectName user-api
 * @date 2020/1/19 17:57
 */
public class LockConstant {
    /**
     * ldap 用户登录
     */
    public static final String LDAP_LOGIN = "ldap:login:";
}
