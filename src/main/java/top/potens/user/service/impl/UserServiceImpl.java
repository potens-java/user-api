package top.potens.user.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;
import top.potens.core.annotation.Lock;
import top.potens.core.enums.LockModel;
import top.potens.core.exception.ApiException;
import top.potens.core.model.TokenUser;
import top.potens.core.util.BeanCopierUtil;
import top.potens.core.util.TokenUtil;
import top.potens.log.AppLogger;
import top.potens.user.bmo.Person;
import top.potens.user.bmo.UserMoreAuthBo;
import top.potens.user.code.UserCode;
import top.potens.user.common.constant.ChannelConstant;
import top.potens.user.common.constant.LockConstant;
import top.potens.user.config.ApolloConfiguration;
import top.potens.user.curd.UserAuthCurd;
import top.potens.user.curd.UserCurd;
import top.potens.user.mapper.PersonAttributeMapper;
import top.potens.user.model.User;
import top.potens.user.model.UserAuth;
import top.potens.user.pattern.login.UUIDLogin;
import top.potens.user.request.UserLoginCommonRequest;
import top.potens.user.service.UserService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserServiceImpl
 * @projectName user-api
 * @date 2020/1/19 17:18
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {
    private final UUIDLogin uuidLogin;

    @Override
    public String commonLogin(UserLoginCommonRequest request) {
        if (ChannelConstant.ChannelCode.SELF_VISITOR.equals(request.getChannelCode())) {
            return uuidLogin.auto(request);
        }
        return null;
    }
}
