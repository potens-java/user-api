package top.potens.user.service;

import top.potens.user.model.UserAuth;
import top.potens.user.request.UserLoginCommonRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserService
 * @projectName user-api
 * @date 2019/10/25 12:08
 */
public interface UserService {
    /**
    *
    * 方法功能描述:
    *
    * @author yanshaowen
    * @date 2020/1/28 14:54
    * @param request
    * @return
    * @throws
    */
    String commonLogin(UserLoginCommonRequest request);
}
