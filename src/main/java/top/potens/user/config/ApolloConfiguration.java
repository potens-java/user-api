package top.potens.user.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className ApolloConfiguration
 * @projectName user-api
 * @date 2019/8/24 10:17
 */
@Configuration
@Data
public class ApolloConfiguration {
    @Value("${user.connection.ldap.ou.user}")
    private String ldapOuUser;

    @Value("${user.connection.ldap.ou.group}")
    private String ldapOuGroup;

    @Value("${user.connection.ldap.identifier:uid}")
    private String ldapIdentifier;
}
