package top.potens.user.request;

import lombok.Data;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserLoginCommonRequest
 * @projectName user-api
 * @date 2020/1/28 14:52
 */
@Data
public class UserLoginCommonRequest {
    private String identifier;
    private String channelCode;
    private String credential;
}
