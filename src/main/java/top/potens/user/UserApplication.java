package top.potens.user;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import top.potens.core.response.ResultCodeInit;

@SpringBootApplication(scanBasePackages = {"top.potens"})
@EnableTransactionManagement
@EnableApolloConfig
@EnableFeignClients(basePackages = {"top.potens"})
public class UserApplication {

    public static void main(String[] args) {
        ResultCodeInit.addResultCodeDefinitionClassByPackage("top.potens.user.code");
        SpringApplication.run(UserApplication.class, args);
    }
}
