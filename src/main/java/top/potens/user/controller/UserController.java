package top.potens.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.potens.core.model.ApiResult;
import top.potens.user.common.constant.ChannelConstant;
import top.potens.user.dao.client.CmsClient;
import top.potens.user.dao.client.response.ChannelDetailResponse;
import top.potens.user.request.UserLoginCommonRequest;
import top.potens.user.service.UserService;

import javax.validation.constraints.NotNull;


/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className UserController
 * @projectName user-api
 * @date 2019/7/5 13:36
 */
@RestController
@RequestMapping("/user")
@Api(description = "用户基本操作")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Validated
public class UserController {
    private final UserService userService;
    @GetMapping("/visitor/login")
    @ApiOperation(value = "游客用户登录注册接口")
    public ApiResult<String> userVisitorLogin(
            @ApiParam(value = "identifier", example = "1") @RequestParam(required = true) @NotNull String identifier
    ) {
        ApiResult<String> result = new ApiResult<>();
        UserLoginCommonRequest userLoginCommonRequest = new UserLoginCommonRequest();
        userLoginCommonRequest.setChannelCode(ChannelConstant.ChannelCode.SELF_VISITOR);
        userLoginCommonRequest.setIdentifier(identifier);
        result.setData(userService.commonLogin(userLoginCommonRequest));
        return result;
    }

}
