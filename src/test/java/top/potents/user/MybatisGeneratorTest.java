package top.potent.test;

import org.junit.jupiter.api.Test;
import top.potens.generator.MybatisGenerator;

/**
 * 功能描述:
 *
 * @author yanshaowen
 * @className MybatisGeneratorTest
 * @projectName testpapioframework
 * @date 2020/1/16 15:54
 */
public class MybatisGeneratorTest {
    @Test
    void run() throws Exception {
        MybatisGenerator mybatisGenerator = MybatisGenerator.getInstance();
        mybatisGenerator.setJdbcConnectionURL("jdbc:mysql://127.0.0.1:3306/user?useSSL=false&serverTimezone=Hongkong")
                .setJdbcDriverClass("com.mysql.cj.jdbc.Driver")
                .setJdbcUserId("readonly")
                .setJdbcPassword("314106")
                .setModelTargetPackage("top.potens.user.model")
                .setModelTargetProject("src/main/java")
                .setMapTargetPackage("auto")
                .setMapTargetProject("src/main/resources/mappings")
                .setClientTargetPackage("top.potens.user.dao.db.auto")
                .setClientTargetProject("src/main/java")
                .addTable("t_user", "user_id", "User")
                .addTable("t_user_auth", "user_auth_id", "UserAuth")
                .run();
    }
}
